# encoding: utf-8

class BannerUploader < CarrierWave::Uploader::Base
    include CarrierWave::RMagick

    # Choose what kind of storage to use for this uploader:
    storage :file

    # Override the directory where uploaded files will be stored.
    # This is a sensible default for uploaders that are meant to be mounted:
    def store_dir
        "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
    end

    process resize_to_fill: [610, 270]
end
