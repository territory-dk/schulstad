class SubmissionsController < ApplicationController
  def new
    @submission = Submission.new
  end

  def create
    @submission = Submission.new(submission_params)

    if @submission.save
      redirect_to submission_success_path
    else
      render 'new'
    end
  end

  def success
  end

  def submission_params
    params.require(:submission).permit(:school_name, :contact_person, :address, :zip, :city, :email, :class_level, :students, :message)
  end
end
