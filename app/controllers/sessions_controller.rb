class SessionsController < ApplicationController
  before_filter :authorize, except: [:new, :create]

  def new
  end

  def create
    user = User.find_by_name(session_params[:name])
    if user && user.authenticate(session_params[:password])
      session[:user_id] = user.id
      redirect_to admin_root_path, success: "Welcome, #{user.name}"
    else
      flash.now[:error] = 'Invalid password or email'
      render 'new'
    end
  end

  def destroy
    session[:user_id] = nil
    redirect_to root_path, success: 'You have been logged out'
  end

  private

  def session_params
    params.require(:session).permit(:name, :password)
  end
end