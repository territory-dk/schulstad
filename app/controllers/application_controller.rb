class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  def authenticate_user!
    unless session[:user_id].present?
      redirect_to login_path
    end
  end

  helper_method :current_user
  def current_user
    User.find(session[:user_id]) if session[:user_id].present?
  end

  def authorize
    redirect_to login_url, warning: 'Not authorized' if current_user.nil?
  end
end
