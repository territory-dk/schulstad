class ArticlesController < ApplicationController
  def show
    unless params[:id].is_number?
      @article = Article.find_by_slug(params[:id])
    end
    @article ||= Article.find params[:id]

    raise ActiveRecord::RecordNotFound.new if @article.nil?

    @active_cat = @article.category
    @quiz = @article.quiz
    @quiz ||= @article.category.quizzes.order("RANDOM()").first
  end
end
