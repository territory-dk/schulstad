class ArticleSection < ActiveRecord::Base
  belongs_to :article
  default_scope { order(:weight) }

  mount_uploader :image, SectionUploader
end
