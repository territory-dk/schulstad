class RecipeCategory < ActiveRecord::Base
  has_many :recipes

  default_scope { order(:weight) }
end
