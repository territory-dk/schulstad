class Category < ActiveRecord::Base
  has_many :articles
  has_many :quizzes

  default_scope { order(:weight) }
end
