class Recipe < ActiveRecord::Base
  default_scope { order(:weight) }
  mount_uploader :image, RecipeUploader

  belongs_to :recipe_category
end
