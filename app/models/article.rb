class Article < ActiveRecord::Base
  belongs_to :category
  has_many :article_sections
  has_one :quiz

  accepts_nested_attributes_for :article_sections, allow_destroy: true
  serialize :entryboxes, Hash

  mount_uploader :image, BannerUploader

  default_scope { order(:weight) }

  before_save :generate_slug

  def generate_slug
    self.slug = self.title.parameterize
  end

  def get_entryboxes
    boxes = {}
    types = [:quiz_entry, :exercise_entry, :wholegrain_entry, :food_entry, :recipes_entry]

    types.each do |t|
      boxes[t] = send(t)
    end

    boxes.sort_by { |_, v| -v }.reject { |_,v| v == 0 }.reverse
  end
end
