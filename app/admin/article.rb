ActiveAdmin.register Article do

  permit_params do
    permitted = [:permitted, :title, :sub_title, :category_id, :body, :image, :weight, :quiz_entry, :exercise_entry, :wholegrain_entry, :food_entry, :recipes_entry, :_destroy]
    permitted << { article_sections_attributes: [:id, :header, :body, :weight, :image, :image_position, :_destroy] }
    permitted
  end

  index do
    column :title
    column :sub_title
    column :category
    column :created_at

    actions
  end

  form do |f|

    f.inputs do
      f.input :title
      f.input :sub_title
      f.input :category
      f.input :image
      f.input :body, as: :wysihtml5, commands: :all
      f.input :weight

      panel 'Entry boxes' do
        'Enter the weight of the entry box, this determines the order of the boxes. Zero will hide the entrybox.'
      end
      f.input :quiz_entry
      f.input :exercise_entry
      f.input :wholegrain_entry
      f.input :food_entry
      f.input :recipes_entry


      f.has_many :article_sections, sort_by: :weight,  heading: 'Sections', allow_destroy: true, new_record: true do |t|
        t.input :header
        t.input :body, as: :wysihtml5, commands: :all
        t.input :image
        t.input :weight
        t.input :image_position, as: :radio, collection: ['Left', 'Right', 'Full', 'No image']
      end
    end

    f.actions
  end
end
