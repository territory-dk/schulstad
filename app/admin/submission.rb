ActiveAdmin.register Submission do

  permit_params do
    permitted = [:school_name, :contact_person, :address, :zip, :city, :email, :class_level, :students, :message, :_destroy]
    permitted
  end

  index do
    column :school_name
    column :contact_person
    column :city
    column :email

    actions
  end

  form do |f|
    f.inputs
    f.actions
  end
end
