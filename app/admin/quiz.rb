ActiveAdmin.register Quiz do
  permit_params :category_id, :article_id, :question, :answer_1, :answer_2, :answer_3, :correct_answer
end
