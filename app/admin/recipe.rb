ActiveAdmin.register Recipe do
  permit_params do
    permitted = [:permitted, :name, :body, :image, :weight, :recipe_category_id]
    permitted
  end

  form do |f|
    f.inputs do
      f.input :name
      f.input :recipe_category
      f.input :body, as: :wysihtml5, commands: :all
      f.input :image
      f.input :weight
    end

    f.actions
  end
end
