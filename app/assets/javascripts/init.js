app = {};

$(document).on('ready page:load', function() {
  app.menu.init('header.site-header nav.entry-menu > a');
  app.quiz.init();

  $('.open-modal').magnificPopup({
    closeMarkup: '<button title="%title%" type="button" class="mfp-close image-modal__close"></button>',
    type:'inline',
    midClick: true // allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source.
  });
});
