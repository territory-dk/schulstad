app.quiz = (function () {
  var correctAnswer = null;

  var answer = function() {
    if ($(this).data('answer') == correctAnswer) {
      showBlock('correct');
    } else {
      showBlock('wrong');
    }
  };

  var showBlock = function(block) {
    $('.quiz__body').hide();
    $('.quiz__body.quiz__body--' + block).show();
  };

  return {
    init: function() {
      correctAnswer = $('#answer').data('answer');

      $('.quiz__answer').click(answer);
      $('.quiz__body--wrong span').click(function() { showBlock('init') });
    }
  }
})();