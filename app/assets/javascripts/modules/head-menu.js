app.menu = (function () {
  var activate = function(e) {
    e.preventDefault();
    // Start by hiding all sub menus.
    $('nav.sub-menu').hide();

    // Set active.
    $('header nav.entry-menu > a').removeClass('active');
    $(this).addClass('active');

    // Find and show new menu.
    var menuId = $(this).data('id')
    $('nav[data-id="' + menuId + '"]').show();
  };

  var expandMenu = function() {
    var currentPath = location.pathname;

    $('.sub-menu a').each(function() {
      if (currentPath == $(this).attr('href')) {
        $(this).parent('nav').show().addClass('active');
        $(this).addClass('active')
      }
    });
  };

  return {
    init: function(selector) {
      expandMenu();

      $(selector).click(activate);
    }
  }
})();
