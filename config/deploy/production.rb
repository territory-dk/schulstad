role :app, %w{deployer@139.162.130.76}
role :web, %w{deployer@139.162.130.76}
role :db,  %w{deployer@139.162.130.76}

server '139.162.130.76', user: 'deployer', roles: %w{web app}

set :domain, 'madpakkensdag.dk'
set :rails_env, 'production'

set :deploy_to, '/home/deployer/schulstad/production'

set :app_port, 39274

set :ssh_options, {
    forward_agent: true
}