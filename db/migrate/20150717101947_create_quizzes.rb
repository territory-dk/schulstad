class CreateQuizzes < ActiveRecord::Migration
  def change
    create_table :quizzes do |t|
      t.string :question
      t.string :answer_1
      t.string :answer_2
      t.string :answer_3
      t.integer :correct_answer
      t.integer :category_id
      t.integer :article_id

      t.timestamps null: false
    end
    add_index :quizzes, :category_id
  end
end
