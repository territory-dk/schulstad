class CreateArticles < ActiveRecord::Migration
  def change
    create_table :articles do |t|
      t.string :title
      t.string :image
      t.text :body
      t.boolean :editable
      t.integer :category_id

      t.timestamps null: false
    end
  end
end
