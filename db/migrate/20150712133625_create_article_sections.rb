class CreateArticleSections < ActiveRecord::Migration
  def change
    create_table :article_sections do |t|
      t.string :image
      t.string :header
      t.text :body
      t.string :image_position
      t.integer :article_id

      t.timestamps null: false
    end
    add_index :article_sections, :article_id
  end
end
