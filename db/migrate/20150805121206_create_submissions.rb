class CreateSubmissions < ActiveRecord::Migration
  def change
    create_table :submissions do |t|
      t.string :school_name
      t.string :contact_person
      t.string :address
      t.integer :zip
      t.string :city
      t.string :email
      t.string :class_level
      t.string :students
      t.text :message

      t.timestamps null: false
    end
  end
end
