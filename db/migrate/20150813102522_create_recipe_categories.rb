class CreateRecipeCategories < ActiveRecord::Migration
  def change
    create_table :recipe_categories do |t|
      t.string :name
      t.integer :weight, default: 0

      t.timestamps null: false
    end
    add_index :recipe_categories, :weight

    RecipeCategory.create(name: 'Morgenmad', weight: 1)
    RecipeCategory.create(name: 'Mellemmåltid', weight: 2)
    RecipeCategory.create(name: 'Madpakker', weight: 3)
  end
end
