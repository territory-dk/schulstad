class CreateRecipes < ActiveRecord::Migration
  def change
    create_table :recipes do |t|
      t.string :name
      t.text :body
      t.integer :weight, default: 0

      t.timestamps null: false
    end
    add_index :recipes, :weight
  end
end
