class AddSlugsAndSubtitle < ActiveRecord::Migration
  def change
    add_column :articles, :slug, :string
    add_column :articles, :sub_title, :string
  end
end
