class AddEntryBoxConfFieldsToArticles < ActiveRecord::Migration
  def change
    add_column :articles, :quiz_entry, :integer, default: 0
    add_column :articles, :exercise_entry, :integer, default: 0
    add_column :articles, :wholegrain_entry, :integer, default: 0
    add_column :articles, :food_entry, :integer, default: 0
    add_column :articles, :recipes_entry, :integer, default: 0
  end
end
