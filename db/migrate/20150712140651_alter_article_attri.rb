class AlterArticleAttri < ActiveRecord::Migration
  def change
    remove_column :articles, :editable, :boolean
    add_column :article_sections, :weight, :integer, default: 0
    add_index :article_sections, :weight
  end
end
