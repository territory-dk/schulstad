class AddWeightToArticles < ActiveRecord::Migration
  def change
    add_column :articles, :weight, :integer
    add_index :articles, :weight
  end
end
