# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#

# Create the require categories.
Category.create name: 'Værd at vide', weight: 0
Category.create name: 'Fra mark til brød', weight: 1
Category.create name: 'Det du spiser', weight: 2
Category.create name: 'Hvad maden gør', weight: 3
Category.create name: 'Inspiration', weight: 4
Category.create name: 'Til læren', weight: 5
