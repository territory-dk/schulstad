# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150813102716) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "active_admin_comments", force: :cascade do |t|
    t.string   "namespace"
    t.text     "body"
    t.string   "resource_id",   null: false
    t.string   "resource_type", null: false
    t.integer  "author_id"
    t.string   "author_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id", using: :btree
  add_index "active_admin_comments", ["namespace"], name: "index_active_admin_comments_on_namespace", using: :btree
  add_index "active_admin_comments", ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id", using: :btree

  create_table "article_sections", force: :cascade do |t|
    t.string   "image"
    t.string   "header"
    t.text     "body"
    t.string   "image_position"
    t.integer  "article_id"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.integer  "weight",         default: 0
  end

  add_index "article_sections", ["article_id"], name: "index_article_sections_on_article_id", using: :btree
  add_index "article_sections", ["weight"], name: "index_article_sections_on_weight", using: :btree

  create_table "articles", force: :cascade do |t|
    t.string   "title"
    t.string   "image"
    t.text     "body"
    t.integer  "category_id"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.string   "slug"
    t.string   "sub_title"
    t.integer  "weight"
    t.integer  "quiz_entry",       default: 0
    t.integer  "exercise_entry",   default: 0
    t.integer  "wholegrain_entry", default: 0
    t.integer  "food_entry",       default: 0
    t.integer  "recipes_entry",    default: 0
  end

  add_index "articles", ["weight"], name: "index_articles_on_weight", using: :btree

  create_table "assets", force: :cascade do |t|
    t.string   "storage_uid"
    t.string   "storage_name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "storage_width"
    t.integer  "storage_height"
    t.float    "storage_aspect_ratio"
    t.integer  "storage_depth"
    t.string   "storage_format"
    t.string   "storage_mime_type"
    t.string   "storage_size"
  end

  create_table "categories", force: :cascade do |t|
    t.string   "name"
    t.integer  "weight"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "quizzes", force: :cascade do |t|
    t.string   "question"
    t.string   "answer_1"
    t.string   "answer_2"
    t.string   "answer_3"
    t.integer  "correct_answer"
    t.integer  "category_id"
    t.integer  "article_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  add_index "quizzes", ["category_id"], name: "index_quizzes_on_category_id", using: :btree

  create_table "recipe_categories", force: :cascade do |t|
    t.string   "name"
    t.integer  "weight",     default: 0
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "recipe_categories", ["weight"], name: "index_recipe_categories_on_weight", using: :btree

  create_table "recipes", force: :cascade do |t|
    t.string   "name"
    t.text     "body"
    t.integer  "weight",             default: 0
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.string   "image"
    t.integer  "recipe_category_id"
  end

  add_index "recipes", ["weight"], name: "index_recipes_on_weight", using: :btree

  create_table "submissions", force: :cascade do |t|
    t.string   "school_name"
    t.string   "contact_person"
    t.string   "address"
    t.integer  "zip"
    t.string   "city"
    t.string   "email"
    t.string   "class_level"
    t.string   "students"
    t.text     "message"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "name"
    t.string   "password_digest"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

end
